PROJECT_LIST_SERVICES += kibana

KIBANA_COMPOSE_FILE ?= $(RESOURCES)/kibana/docker-compose
COMPOSE_FILE += -f $(KIBANA_COMPOSE_FILE).yml -f $(KIBANA_COMPOSE_FILE).$(ENV).yml

TARGETS_HELP += kibana-help

PRE_TARGETS_ENV+=kibana-env

PRE_TARGETS_UP+=kibana-up
PRE_TARGETS_START+=kibana-start
PRE_TARGETS_STOP+=kibana-stop
PRE_TARGETS_DOWN+=kibana-down

#ES_HOST ?= redis
#ES_BASEs ?= 0
#ES_PASS ?= example
#ES_MAX_CLIENTS ?= 10000
#ES_MAX_MEMORY ?= 0

kibana-help:
	$(call SHOW_TITLE_HELP, KIBANA)
	$(call SHOW_CMD_HELP, kibana-up) Запускаем окружение UP \(KIBANA\)
	$(call SHOW_CMD_HELP, kibana-start) Запускаем/стартуем остановленное приложение \(KIBANA\)
	$(call SHOW_CMD_HELP, kibana-stop) Остановлеваем приложение \(KIBANA\)
	$(call SHOW_CMD_HELP, kibana-rm) удаляем остановленное приложение \(KIBANA\)
	$(call SHOW_CMD_HELP, kibana-log) логи приложение \(KIBANA\)

kibana-up:
	@docker-compose -f $(COMPOSE_FILE) up -d kibana

kibana-start:
	@docker-compose -f $(COMPOSE_FILE) start kibana

kibana-stop:
	@docker-compose -f $(COMPOSE_FILE) stop kibana

kibana-rm:
	@docker-compose -f $(COMPOSE_FILE) rm -s kibana

kibana-down: kibana-stop kibana-rm

kibana-log:
	@docker logs kibana

kibana-env:
	@echo "# ---- KIBANA ----" >> .env
	@echo "" >> .env
